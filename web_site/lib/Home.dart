import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:web_site/SD.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.sizeOf(context).height,
      color: const Color(0xFF0E0E2E),
      // decoration: const BoxDecoration(
      //   image: DecorationImage(
      //     image: AssetImage('images/lines.png'),
      //     fit: BoxFit.cover,
      //   ),
      // ),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            const SizedBox(height: 6),
            Header(),
            const SizedBox(height: 0),
            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 75.0), // Adjust as needed
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'A  perfect  combination  of  Tech',
                      style: TextStyle(
                        fontSize: 35,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    const Text(
                      'and  Hands-on  kits  to  promote',
                      style: TextStyle(
                        fontSize: 35,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    const Text(
                      'Experiential  Learning',
                      style: TextStyle(
                        fontSize: 35,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    const Text(
                      'Here Innovation Fuels Engine',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                        color: Colors.grey,
                      ),
                    ),
                    const SizedBox(height: 20),
                    ElevatedButton(
                      onPressed: () {},
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.blue,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 10),
                      ),
                      child: const Text('Know more >'),
                    ),
                    // const SizedBox(height: 20),
                  ],
                ),
              ),
            ),
            // const SizedBox(
            //   height: 20,
            // ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 100),
                  child: Image.asset(
                    "images/rocket.png",
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 100),
                  child: Image.asset(
                    "images/student.png",
                    width: MediaQuery.of(context).size.width *
                        0.35, // Larger width
                    height: MediaQuery.of(context).size.height *
                        0.55, // Larger heig
                  ),
                ),
              ],
            ),

            Column(children: [
              Padding(
                padding: const EdgeInsets.all(55.0),
                child: SizedBox(
                  width: 172,
                  height: 45,
                  child: ElevatedButton(
                    onPressed: () {},
                    style: ElevatedButton.styleFrom(
                      foregroundColor: Colors.white,
                      backgroundColor: Colors.transparent,
                      shadowColor: Colors.transparent,
                      side: const BorderSide(color: Colors.white),
                    ),
                    child: const Text('About'),
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              const Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsets.only(left: 70.0), // Adjust as needed
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'We offer a dynamic blend of technology and hands-on kits to foster experiential learning. Our programs empowe',
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(162, 161, 183, 1)),
                      ),
                      Text(
                        'students to explore the realms of science, technology, engineering, and mathematics (STEM) in engaging way',
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(162, 161, 183, 1)),
                      ),
                      Text(
                        'From robotics and coding to chemistry experiments, our comprehensive approach nurtures creativity and critical',
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(162, 161, 183, 1)),
                      ),
                      Text(
                        'thinking. Join us in this educational transformation and unlock your potential today!',
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(162, 161, 183, 1)),
                      ),
                      SizedBox(height: 30),

                      // const SizedBox(height: 20),
                    ],
                  ),
                ),
              ),
            ]),
            const SizedBox(
              height: 20,
            ),

            Center(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Container(
                  width: 160,
                  height: 45,
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                    border: Border.all(color: Colors.white),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: Center(
                    child: Image.asset(
                      'images/aboutframe.png', // Updated this path
                      width: 140,
                      height: 140,
                    ),
                  ),
                ),
              ),
            ),

//4444444444444444444444444444444444444444444444444444444444444444
            Padding(
              padding: const EdgeInsets.all(55.0),
              child: SizedBox(
                width: 172,
                height: 45,
                child: ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: Colors.transparent,
                    shadowColor: Colors.transparent,
                    side: const BorderSide(color: Colors.white),
                  ),
                  child: const Text('Feature'),
                ),
              ),
            ),

//mmmmmmmmmmmmmmmmmmmmmmmmmmyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy

/////////////////////////////////////////////////////////////////
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(width: 40), // Add more space on the left side
                Expanded(
                  flex: 1,
                  child: Container(
                    height: 450,
                    decoration: BoxDecoration(
                      color: Colors.transparent,
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const SizedBox(height: 25),
                        const Text(
                          "Kit + Mobile Application",
                          style: TextStyle(
                            fontSize: 23,
                            fontWeight: FontWeight.w700,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(height: 15),
                        const Padding(
                          padding: EdgeInsets.all(16.0),
                          child: Text(
                            'A perfect combination of Technology and hands-on kits to promote experiential learning',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        const SizedBox(height: 15),
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.transparent,
                              border: Border.all(
                                color: const Color.fromARGB(59, 86, 141, 186),
                              ),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Center(
                              child: Image.asset(
                                "images/459shots_so 1.png",
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                    width: 60), // Add more space between the containers
                Expanded(
                  flex: 2, // Adjusted flex value
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Image.asset(
                      "images/stdimg-Photoroom.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                const SizedBox(
                    width: 60), // Add more space between the containers
                Expanded(
                  flex: 1,
                  child: Container(
                    height: 450,
                    decoration: BoxDecoration(
                      color: Colors.transparent,
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const SizedBox(height: 25),
                        const Text(
                          "Profile Page + Contest Page",
                          style: TextStyle(
                            fontSize: 23,
                            fontWeight: FontWeight.w700,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(height: 15),
                        const Padding(
                          padding: EdgeInsets.all(16.0),
                          child: Text(
                            'Ultimate application for upgrading skills and learning more',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        const SizedBox(height: 15),
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.transparent,
                              border: Border.all(
                                color: const Color.fromARGB(59, 86, 141, 186),
                              ),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Center(
                              child: Image.asset(
                                "images/629shots_so 1.png",
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(width: 40), // Add more space on the right side
              ],
            ),

//44444444444444444444444444444444
            const SizedBox(
              height: 30,
            ),
            Image.asset(
              "images/feafoot.png",
              height: 200,
              width: 900,
            ),

            const SizedBox(
              height: 100,
            ),

            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(width: 80),
                const Expanded(
                  flex: 2,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Learn Without\nlimits',
                        style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(height: 15),
                      Text(
                        'Start, switch, or advance your career with more than 7,000 courses, Professional Certificates, and degrees from world-class universities and companies.',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 40),
                Expanded(
                  flex: 3,
                  child: Container(
                    //constraints: BoxConstraints(maxWidth: 400),
                    height: 250,
                    width: 50,
                    decoration: BoxDecoration(
                      color: Colors.transparent,
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    child: Image.asset(
                      "images/backtoschool.png", // Adjust the path according to your project
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 50,
                )
              ],
            ),

            const SizedBox(
              height: 100,
            ),
//33333333333333333333333333333333333333333333333333333333333333333333333333333333333333

            Container(
              margin: const EdgeInsets.symmetric(horizontal: 150.0),
              padding: const EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                color: Colors.transparent,
                border: Border.all(color: Colors.white),
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: LayoutBuilder(
                builder: (context, constraints) {
                  return Row(
                    children: [
                      const SizedBox(
                        width: 10,
                      ),
                      Image.asset(
                        "images/2mobiles.png",
                        width: constraints.maxWidth * 0.4, // Responsive width
                        fit: BoxFit.contain,
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(
                              height: 20,
                            ),
                            const Text(
                              '''Expand your skills &
knowledge at any level''',
                              style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                              ),
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            const Text(
                              '''We have easily downloadable learning app on any device and anywhere
to experience an exclusive e-learning platform for world-class education''',
                              style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w400,
                                color: Colors.grey,
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: SizedBox(
                                      width: 130,
                                      height: 45,
                                      child: ElevatedButton(
                                        onPressed: () {},
                                        style: ElevatedButton.styleFrom(
                                          foregroundColor: Colors.white,
                                          backgroundColor: Colors.transparent,
                                          shadowColor: Colors.transparent,
                                          side: const BorderSide(
                                              color: Colors.white),
                                        ),
                                        child: const Text('Contact Us'),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: GestureDetector(
                                      onTap: () {
                                        // Define your onTap action here
                                      },
                                      child: Container(
                                        width: 130,
                                        height: 45,
                                        decoration: BoxDecoration(
                                          color: Colors.transparent,
                                          border:
                                              Border.all(color: Colors.white),
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                        ),
                                        child: Center(
                                          child: Image.asset(
                                            'images/appstore.png', // Correct the path
                                            width: 140,
                                            height: 140,
                                            fit: BoxFit.contain,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: GestureDetector(
                                      onTap: () {
                                        // Define your onTap action here
                                      },
                                      child: Container(
                                        width: 130,
                                        height: 45,
                                        decoration: BoxDecoration(
                                          color: Colors.transparent,
                                          border:
                                              Border.all(color: Colors.white),
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                        ),
                                        child: Center(
                                          child: Image.asset(
                                            'images/gp1.png', // Correct the path
                                            width: 140,
                                            height: 140,
                                            fit: BoxFit.contain,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),
            const SizedBox(
              height: 60,
            ),

            Container(
              margin: const EdgeInsets.symmetric(
                  horizontal: 20.0), // Adjust horizontal margin as needed
              child: const Divider(
                color: Colors.white,
                thickness: 1.0, // Adjust thickness as needed
              ),
            ),
            const Footer(),
            const SizedBox(height: 10),
            // Add additional widgets like the About button or images here
          ],
        ),
      ),
    );
  }
}

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.all(50.0),
          child: Image.asset(
            'images/iinsparkeduskills.png',
            width: 100,
            height: 50,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(50.0),
          child: ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const SignUpDesk()),
              );
            },
            style: ElevatedButton.styleFrom(
              foregroundColor: Colors.white,
              backgroundColor: Colors.transparent,
              shadowColor: Colors.transparent,
              side: const BorderSide(color: Colors.white),
            ),
            child: const Text('Register'),
          ),
        ),
      ],
    );
  }
}

class Footer extends StatelessWidget {
  const Footer({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 40.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.asset(
                    'images/iinsparkeduskills.png', // Update this path
                    width: 90,
                    height: 60,
                  ),
                  const SizedBox(height: 10),
                  const Text(
                    '''We are strategic & creative digital agency who are 
focused on user experience, mobile, social, data 
gathering and promotional offerings.''',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                    ),
                  ),
                  const SizedBox(
                    height: 25,
                  ),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          // Handle Instagram redirection
                          // Example: You can use a package like url_launcher to open URLs
                          // Make sure to import the package: import 'package:url_launcher/url_launcher.dart';
                          // Example:
                          // launch('https://www.instagram.com/your_instagram_account');
                        },
                        child: Image.asset(
                          "images/instagram.png",
                          height: 35,
                          width: 35,
                        ),
                      ),
                      const SizedBox(width: 15),
                      GestureDetector(
                        onTap: () {
                          // Handle Twitter redirection
                          // Example: You can use a package like url_launcher to open URLs
                          // Make sure to import the package: import 'package:url_launcher/url_launcher.dart';
                          // Example:
                          // launch('https://twitter.com/your_twitter_account');
                        },
                        child: Image.asset(
                          "images/twitter.png",
                          height: 35,
                          width: 35,
                        ),
                      ),
                      const SizedBox(width: 15),
                      GestureDetector(
                        onTap: () {
                          // Handle Facebook redirection
                          // Example: You can use a package like url_launcher to open URLs
                          // Make sure to import the package: import 'package:url_launcher/url_launcher.dart';
                          // Example:
                          // launch('https://www.facebook.com/your_facebook_account');
                        },
                        child: Image.asset(
                          "images/fb.png",
                          height: 35,
                          width: 35,
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 25,
                  ),
                  const Text(
                    '© 2024 IINSPARK EDUSKILLS. All rights reserved.',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}

// import 'package:flutter/material.dart';
// import 'package:web_site/Home.dart';
// import 'package:web_site/LoginDesk.dart';

class SignUpDesk extends StatefulWidget {
  const SignUpDesk({super.key});

  @override
  State<SignUpDesk> createState() => _SignUpDeskState();
}

class _SignUpDeskState extends State<SignUpDesk> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LayoutBuilder(
        builder: (context, constraints) {
          return Container(
            height: constraints.maxHeight,
            color: const Color(0xFF0E0E2E),
            child: Center(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    children: [
                      // Place the Footer at the top
                      const SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Image.asset(
                              'images/iinsparkeduskills.png',
                              width: 100,
                              height: 50,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 20),
                      const Text(
                        "Register Now",
                        style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        ),
                      ),
                      const SizedBox(height: 20),
                      Row(
                        children: [
                          Image.asset(
                            "images/signupman.png",
                            width: 430,
                            height: 320,
                          ),
                          const SizedBox(width: 20),
                          Flexible(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                _buildTextFormField(
                                  hintText: "Full Name",
                                  icon: Icons.person,
                                  obscureText: false,
                                ),
                                const SizedBox(height: 20),
                                _buildTextFormField(
                                  hintText: "Email Address",
                                  icon: Icons.email,
                                  obscureText: false,
                                ),
                                const SizedBox(height: 20),
                                _buildTextFormField(
                                  hintText: "+91",
                                  icon: Icons.phone,
                                  obscureText: false,
                                ),
                                const SizedBox(height: 20),
                                _buildTextFormField(
                                  hintText: "Password",
                                  icon: Icons.lock,
                                  obscureText: true,
                                ),
                                const SizedBox(height: 20),
                                _buildTextFormField(
                                  hintText: "Confirm Password",
                                  icon: Icons.lock,
                                  obscureText: true,
                                ),
                                const SizedBox(height: 20),
                                SizedBox(
                                  width: 460,
                                  child: ElevatedButton(
                                    onPressed: () {
                                      // Handle sign-up action
                                    },
                                    style: ElevatedButton.styleFrom(
                                      backgroundColor: const Color(0xFF6200EE),
                                      minimumSize:
                                          const Size(double.infinity, 50),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                    ),
                                    child: const Text(
                                      'Sign up',
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 20),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => LoginDesk()),
                                    );
                                  },
                                  child: const Text(
                                    'Already a member? Log in',
                                    style: TextStyle(
                                      color: Colors.white,
                                      decoration: TextDecoration.underline,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      const SizedBox(
                        width: 2000, // Set the desired width
                        child: Divider(
                          color: Colors.white,
                          thickness:
                              1, // Optionally, you can also increase the thickness
                        ),
                      ),
                      const Footer(),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _buildTextFormField({
    required String hintText,
    required IconData icon,
    required bool obscureText,
  }) {
    return SizedBox(
      width: 460,
      child: TextFormField(
        obscureText: obscureText,
        decoration: InputDecoration(
          hintText: hintText,
          hintStyle: const TextStyle(color: Colors.white),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          suffixIcon: Icon(icon, color: Colors.white),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: const BorderSide(color: Colors.white),
          ),
        ),
        style: const TextStyle(color: Colors.white),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "Please enter $hintText";
          }
          return null;
        },
      ),
    );
  }
}

class LoginDesk extends StatefulWidget {
  const LoginDesk({super.key});

  @override
  State<LoginDesk> createState() => _LoginDeskState();
}

class _LoginDeskState extends State<LoginDesk> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LayoutBuilder(
        builder: (context, constraints) {
          return Container(
            height: MediaQuery.sizeOf(context).height,
            color: const Color(0xFF0E0E2E),
            child: Center(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 35),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Image.asset(
                              'images/iinsparkeduskills.png',
                              width: 100,
                              height: 50,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 20),
                      const Text(
                        "Log In",
                        style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        ),
                      ),
                      const SizedBox(height: 20),
                      Row(
                        children: [
                          Image.asset(
                            "images/signupman.png",
                            width: 430,
                            height: 320,
                          ),
                          const SizedBox(width: 20),
                          Flexible(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                _buildTextFormField(
                                  hintText: "Email Address",
                                  icon: Icons.email,
                                  obscureText: false,
                                ),
                                const SizedBox(height: 20),
                                _buildTextFormField(
                                  hintText: "Password",
                                  icon: Icons.lock,
                                  obscureText: true,
                                ),
                                const SizedBox(height: 20),
                                SizedBox(
                                  width: 460,
                                  child: ElevatedButton(
                                    onPressed: () {
                                      // Handle login action
                                    },
                                    style: ElevatedButton.styleFrom(
                                      backgroundColor: const Color(0xFF6200EE),
                                      minimumSize:
                                          const Size(double.infinity, 50),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                    ),
                                    child: const Text(
                                      'Login',
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 20),
                                GestureDetector(
                                  onTap: () {
                                    // Handle forget password
                                  },
                                  child: const Text(
                                    'Forget Password',
                                    style: TextStyle(
                                      color: Colors.white,
                                      decoration: TextDecoration.underline,
                                    ),
                                  ),
                                ),
                                const SizedBox(height: 10),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              const SignUpDesk()),
                                    );
                                  },
                                  child: const Text(
                                    "New to this site? Sign up..!",
                                    style: TextStyle(
                                      color: Colors.white,
                                      decoration: TextDecoration.underline,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        width: 2000, // Set the desired width
                        child: Divider(
                          color: Colors.white,
                          thickness:
                              1, // Optionally, you can also increase the thickness
                        ),
                      ),
                      const Footer(),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _buildTextFormField({
    required String hintText,
    required IconData icon,
    required bool obscureText,
  }) {
    return SizedBox(
      width: 460,
      child: TextFormField(
        obscureText: obscureText,
        decoration: InputDecoration(
          hintText: hintText,
          hintStyle: const TextStyle(color: Colors.white),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          suffixIcon: Icon(icon, color: Colors.white),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: const BorderSide(color: Colors.white),
          ),
        ),
        style: const TextStyle(color: Colors.white),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "Please enter $hintText";
          }
          return null;
        },
      ),
    );
  }
}
