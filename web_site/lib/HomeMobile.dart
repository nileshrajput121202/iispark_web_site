import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:web_site/SM.dart';

class HomeMobile extends StatefulWidget {
  const HomeMobile({super.key});

  @override
  State<HomeMobile> createState() => _HomeMobileState();
}

class _HomeMobileState extends State<HomeMobile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      color: const Color(0xFF0E0E2E),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            const SizedBox(height: 30),
            const Header(), // Replace this with your actual header widget
            const SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'A perfect combination of Tech',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  const Text(
                    'and Hands-on kits to promote',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  const Text(
                    'Experiential Learning',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  const Text(
                    'Here Innovation Fuels Engine',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: Colors.grey,
                    ),
                  ),
                  const SizedBox(height: 10),
                  ElevatedButton(
                    onPressed: () {},
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.blue,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 10),
                    ),
                    child: const Text('Know more >'),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 110),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                      right: 8.0), // Adjust the spacing as needed
                  child: Image.asset(
                    "images/rocket.png",
                    height: 70,
                    width: 70,
                  ),
                ),
                Expanded(
                  child: Image.asset(
                    "images/student.png",
                    width: MediaQuery.of(context).size.width * 0.6,
                    // Adjust size accordingly
                  ),
                ),
              ],
            ),

            const SizedBox(
              height: 50,
            ),

            Column(
              children: [
                Padding(
                  padding:
                      const EdgeInsets.all(16.0), // Reduced padding for mobile
                  child: SizedBox(
                    width: 150,
                    height: 45,
                    child: ElevatedButton(
                      onPressed: () {},
                      style: ElevatedButton.styleFrom(
                        foregroundColor: Colors.white,
                        backgroundColor: Colors.transparent,
                        shadowColor: Colors.transparent,
                        side: const BorderSide(color: Colors.white),
                      ),
                      child: const Text('About'),
                    ),
                  ),
                ),
                const SizedBox(height: 15),
                const Column(
                  children: [
                    SizedBox(height: 15),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal:
                              16.0), // Reduced horizontal padding for mobile
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment
                            .start, // Align text to the left for better readability
                        children: [
                          Text(
                            'We offer a dynamic blend of technology and hands-on kits to foster experiential learning. Our programs empower students to explore the realms of science, technology, engineering, and mathematics (STEM) in engaging ways.',
                            style: TextStyle(
                              fontSize: 16, // Reduced font size for mobile
                              fontWeight: FontWeight.w400,
                              color: Color.fromRGBO(162, 161, 183, 1),
                            ),
                          ),
                          SizedBox(height: 10), // Add space between text blocks
                          Text(
                            'From robotics and coding to chemistry experiments, our comprehensive approach nurtures creativity and critical thinking.',
                            style: TextStyle(
                              fontSize: 16, // Reduced font size for mobile
                              fontWeight: FontWeight.w400,
                              color: Color.fromRGBO(162, 161, 183, 1),
                            ),
                          ),
                          SizedBox(height: 10), // Add space between text blocks
                          Text(
                            'Join us in this educational transformation and unlock your potential today!',
                            style: TextStyle(
                              fontSize: 16, // Reduced font size for mobile
                              fontWeight: FontWeight.w400,
                              color: Color.fromRGBO(162, 161, 183, 1),
                            ),
                          ),
                          SizedBox(height: 30),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),

            const SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                width: 172, // Set the width similar to the "About" button
                height: 45, // Set the height similar to the "About" button
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  border: Border.all(color: Colors.white),
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: Center(
                  child: Image.asset(
                    'images/aboutframe.png',
                    width: 140, // Adjust width if necessary
                    height: 35, // Adjust height if necessary
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),

            const SizedBox(height: 20),

            const SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.all(16.0), // Reduced padding for mobile
              child: SizedBox(
                width: 150,
                height: 45,
                child: ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: Colors.transparent,
                    shadowColor: Colors.transparent,
                    side: const BorderSide(color: Colors.white),
                  ),
                  child: const Text('Feature'),
                ),
              ),
            ),

            const SizedBox(
              height: 35,
            ),

            Column(
              children: [
                Padding(
                  padding:
                      const EdgeInsets.all(8.0), // Adjust padding for mobile
                  child: Image.asset(
                    "images/stdimg-Photoroom.png",
                    fit: BoxFit.cover,
                    height: MediaQuery.of(context).size.height *
                        0.4, // Adjust height for mobile
                    width: MediaQuery.of(context).size.width, // Full width
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(
                            8.0), // Adjust padding for mobile
                        child: Container(
                          height: MediaQuery.of(context).size.height *
                              0.35, // Adjust height for mobile
                          decoration: BoxDecoration(
                            color: Colors.transparent,
                            border: Border.all(color: Colors.white),
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const SizedBox(height: 15),
                              const Text(
                                "Kit + Mobile Application",
                                style: TextStyle(
                                  fontSize: 18, // Adjust font size for mobile
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white,
                                ),
                                textAlign:
                                    TextAlign.center, // Center align text
                              ),
                              const SizedBox(height: 10),
                              const Padding(
                                padding: EdgeInsets.symmetric(horizontal: 8.0),
                                child: Text(
                                  'A perfect combination of Technology and hands-on kits to promote experiential learning',
                                  style: TextStyle(
                                    fontSize: 12, // Adjust font size for mobile
                                    fontWeight: FontWeight.w400,
                                    color: Colors.white,
                                  ),
                                  textAlign:
                                      TextAlign.center, // Center align text
                                ),
                              ),
                              const SizedBox(height: 10),
                              Expanded(
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    border: Border.all(
                                      color: const Color.fromARGB(
                                          59, 86, 141, 186),
                                    ),
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Center(
                                    child: Image.asset(
                                      "images/459shots_so 1.png",
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(
                            8.0), // Adjust padding for mobile
                        child: Container(
                          height: MediaQuery.of(context).size.height *
                              0.35, // Adjust height for mobile
                          decoration: BoxDecoration(
                            color: Colors.transparent,
                            border: Border.all(color: Colors.white),
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const SizedBox(height: 15),
                              const Text(
                                "Profile Page + Contest Page",
                                style: TextStyle(
                                  fontSize: 18, // Adjust font size for mobile
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white,
                                ),
                                textAlign:
                                    TextAlign.center, // Center align text
                              ),
                              const SizedBox(height: 10),
                              const Padding(
                                padding: EdgeInsets.symmetric(horizontal: 8.0),
                                child: Text(
                                  'Ultimate application for upgrading skills and learning more',
                                  style: TextStyle(
                                    fontSize: 12, // Adjust font size for mobile
                                    fontWeight: FontWeight.w400,
                                    color: Colors.white,
                                  ),
                                  textAlign:
                                      TextAlign.center, // Center align text
                                ),
                              ),
                              const SizedBox(height: 10),
                              Expanded(
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    border: Border.all(
                                      color: const Color.fromARGB(
                                          59, 86, 141, 186),
                                    ),
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Center(
                                    child: Image.asset(
                                      "images/629shots_so 1.png",
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),

            Image.asset(
              "images/feafoot.png",
              width: MediaQuery.of(context).size.width *
                  0.9, // 90% of screen width
              height: MediaQuery.of(context).size.height *
                  0.2, // 25% of screen height
              fit: BoxFit
                  .contain, // Adjust the fit as necessary (contain, cover, etc.)
            ),

            const SizedBox(height: 20),

            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                    width: 20), // Adjust spacing as needed for mobile view
                const Expanded(
                  flex: 2,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Learn Without\nlimits',
                        style: TextStyle(
                          fontSize: 30, // Adjust font size for mobile
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(height: 10), // Adjust spacing for mobile view
                      Text(
                        'Start, switch, or advance your career with more than 7,000 courses, Professional Certificates, and degrees from world-class universities and companies.',
                        style: TextStyle(
                          fontSize: 14, // Adjust font size for mobile
                          fontWeight: FontWeight.w400,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 20), // Adjust spacing for mobile view
                Expanded(
                  flex: 3,
                  child: Container(
                    height:
                        315, // Increase height for better visibility on mobile
                    decoration: BoxDecoration(
                      color: Colors.transparent,
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    child: Image.asset(
                      "images/backtoschool.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                const SizedBox(width: 20), // Adjust spacing for mobile view
              ],
            ),

            const SizedBox(
              height: 70,
            ),
            //44444444444444444444444444444444444444444
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 20.0),
              padding: const EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                color: Colors.transparent,
                border: Border.all(color: Colors.white),
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: LayoutBuilder(
                builder: (context, constraints) {
                  bool isSmallScreen = constraints.maxWidth <
                      600; // Define small screen condition

                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Image.asset(
                          'images/2mobiles.png', // Replace with your image path
                          width: isSmallScreen
                              ? constraints.maxWidth * 0.9
                              : constraints.maxWidth * 0.4,
                          fit: BoxFit.contain,
                        ),
                      ),
                      const SizedBox(height: 20),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Expand your skills & knowledge at any level',
                            style: TextStyle(
                              fontSize: isSmallScreen ? 20 : 30,
                              fontWeight: FontWeight.w700,
                              color: Colors.white,
                            ),
                          ),
                          const SizedBox(height: 15),
                          const Text(
                            'We have an easily downloadable learning app on any device and anywhere to experience an exclusive e-learning platform for world-class education',
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w400,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 20),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: SizedBox(
                                width: isSmallScreen ? 120 : 140,
                                height: 45,
                                child: ElevatedButton(
                                  onPressed: () {},
                                  style: ElevatedButton.styleFrom(
                                    foregroundColor: Colors.white,
                                    backgroundColor: Colors.transparent,
                                    side: const BorderSide(color: Colors.white),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20.0),
                                    ),
                                  ),
                                  child: const Text('Contact Us'),
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: GestureDetector(
                                onTap: () {
                                  // Define your onTap action here
                                },
                                child: Container(
                                  width: isSmallScreen ? 120 : 140,
                                  height: 45,
                                  decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    border: Border.all(color: Colors.white),
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Center(
                                    child: Image.asset(
                                      'images/appstore.png', // Correct the path
                                      width: 150,
                                      height: 30,
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: GestureDetector(
                                onTap: () {
                                  // Define your onTap action here
                                },
                                child: Container(
                                  width: isSmallScreen ? 120 : 140,
                                  height: 45,
                                  decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    border: Border.all(color: Colors.white),
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Center(
                                    child: Image.asset(
                                      'images/gp1.png', // Correct the path
                                      width: 150,
                                      height: 30,
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),

            const SizedBox(height: 20),

            const SizedBox(height: 20),
            const Divider(color: Colors.white),

            const Footer(),
            //FooterMobile(), // Replace this with your actual footer widget
          ],
        ),
      ),
    );
  }
}

class Header extends StatelessWidget {
  const Header({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.all(45.0),
          child: Image.asset(
            'images/iinsparkeduskills.png',
            width: 90,
            height: 50,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(25.0),
          child: ElevatedButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SignUpMobile()));
            },
            style: ElevatedButton.styleFrom(
              foregroundColor: Colors.white,
              backgroundColor: Colors.transparent,
              shadowColor: Colors.transparent,
              side: const BorderSide(color: Colors.white),
            ),
            child: const Text('Register'),
          ),
        ),
      ],
    );
  }
}

class Footer extends StatelessWidget {
  const Footer({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
      decoration: BoxDecoration(
        color: Colors.transparent,
        border: Border.all(color: Colors.white),
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            'images/iinsparkeduskills.png', // Update this path
            width: 90,
            height: 60,
          ),
          const SizedBox(height: 10),
          const Text(
            'We are a strategic & creative digital agency focused on user experience, mobile, social, data gathering, and promotional offerings.',
            style: TextStyle(
              color: Colors.white,
              fontSize: 14,
            ),
          ),
          const SizedBox(height: 20),
          Row(
            children: [
              GestureDetector(
                onTap: () {
                  // Handle Instagram redirection
                },
                child: Image.asset(
                  "images/instagram.png",
                  height: 35,
                  width: 35,
                ),
              ),
              const SizedBox(width: 15),
              GestureDetector(
                onTap: () {
                  // Handle Twitter redirection
                },
                child: Image.asset(
                  "images/twitter.png",
                  height: 35,
                  width: 35,
                ),
              ),
              const SizedBox(width: 15),
              GestureDetector(
                onTap: () {
                  // Handle Facebook redirection
                },
                child: Image.asset(
                  "images/fb.png",
                  height: 35,
                  width: 35,
                ),
              )
            ],
          ),
          const SizedBox(height: 20),
          const Text(
            '© 2024 IINSPARK EDUSKILLS. All rights reserved.',
            style: TextStyle(
              color: Colors.white,
              fontSize: 12,
            ),
          ),
        ],
      ),
    );
  }
}

class SignUpMobile extends StatefulWidget {
  const SignUpMobile({super.key});

  @override
  State<SignUpMobile> createState() => _SignUpDeskState();
}

class _SignUpDeskState extends State<SignUpMobile> {
  @override
  Widget build(BuildContext context) {
    final isMobile = MediaQuery.of(context).size.width < 600;

    return Scaffold(
      body: LayoutBuilder(
        builder: (context, constraints) {
          return Container(
            height: constraints.maxHeight,
            color: const Color(0xFF0E0E2E),
            child: Center(
              child: SingleChildScrollView(
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: isMobile ? 16 : 100),
                  child: Column(
                    children: [
                      const SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Image.asset(
                              'images/iinsparkeduskills.png',
                              width: 100,
                              height: 50,
                            ),
                          ),
                        ],
                      ),
                      Image.asset(
                        "images/signupman.png",
                        width: 430,
                        height: 320,
                      ),
                      const SizedBox(height: 20),
                      const Text(
                        "Register Now",
                        style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        ),
                      ),
                      const SizedBox(height: 20),
                      isMobile
                          ? Column(
                              children: _buildFormFields(isMobile),
                            )
                          : Row(
                              children: [
                                Image.asset(
                                  "images/signupman.png",
                                  width: 430,
                                  height: 320,
                                ),
                                const SizedBox(width: 20),
                                Flexible(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: _buildFormFields(isMobile),
                                  ),
                                ),
                              ],
                            ),
                      const SizedBox(height: 20),
                      const SizedBox(
                        width: double.infinity,
                        child: Divider(
                          color: Colors.white,
                          thickness: 1,
                        ),
                      ),
                      const Footer(),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  List<Widget> _buildFormFields(bool isMobile) {
    return [
      _buildTextFormField(
        hintText: "Full Name",
        icon: Icons.person,
        obscureText: false,
        isMobile: isMobile,
      ),
      const SizedBox(height: 20),
      _buildTextFormField(
        hintText: "Email Address",
        icon: Icons.email,
        obscureText: false,
        isMobile: isMobile,
      ),
      const SizedBox(height: 20),
      _buildTextFormField(
        hintText: "+91",
        icon: Icons.phone,
        obscureText: false,
        isMobile: isMobile,
      ),
      const SizedBox(height: 20),
      _buildTextFormField(
        hintText: "Password",
        icon: Icons.lock,
        obscureText: true,
        isMobile: isMobile,
      ),
      const SizedBox(height: 20),
      _buildTextFormField(
        hintText: "Confirm Password",
        icon: Icons.lock,
        obscureText: true,
        isMobile: isMobile,
      ),
      const SizedBox(height: 20),
      SizedBox(
        width: isMobile ? double.infinity : 460,
        child: ElevatedButton(
          onPressed: () {
            // Handle sign-up action
          },
          style: ElevatedButton.styleFrom(
            backgroundColor: const Color(0xFF6200EE),
            minimumSize: const Size(double.infinity, 50),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
          ),
          child: const Text(
            'Sign up',
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
      const SizedBox(height: 20),
      GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const LoginMobile()),
          );
        },
        child: const Text(
          'Already a member? Log in',
          style: TextStyle(
            color: Colors.white,
            decoration: TextDecoration.underline,
          ),
        ),
      ),
    ];
  }

  Widget _buildTextFormField({
    required String hintText,
    required IconData icon,
    required bool obscureText,
    required bool isMobile,
  }) {
    return SizedBox(
      width: isMobile ? double.infinity : 460,
      child: TextFormField(
        obscureText: obscureText,
        decoration: InputDecoration(
          hintText: hintText,
          hintStyle: const TextStyle(color: Colors.white),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          suffixIcon: Icon(icon, color: Colors.white),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: const BorderSide(color: Colors.white),
          ),
        ),
        style: const TextStyle(color: Colors.white),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "Please enter $hintText";
          }
          return null;
        },
      ),
    );
  }
}

class LoginMobile extends StatefulWidget {
  const LoginMobile({Key? key}) : super(key: key);

  @override
  State<LoginMobile> createState() => _LoginMobileState();
}

class _LoginMobileState extends State<LoginMobile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LayoutBuilder(
        builder: (context, constraints) {
          return Container(
            height: MediaQuery.of(context).size.height,
            color: const Color(0xFF0E0E2E),
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const SizedBox(height: 20),
                    Image.asset(
                      'images/iinsparkeduskills.png',
                      width: 150,
                      height: 75,
                    ),
                    const SizedBox(height: 20),
                    const Text(
                      "Log In",
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                      ),
                    ),
                    const SizedBox(height: 20),
                    Image.asset(
                      "images/signupman.png",
                      width: MediaQuery.of(context).size.width * 0.8,
                    ),
                    const SizedBox(height: 20),
                    _buildTextFormField(
                      hintText: "Email Address",
                      icon: Icons.email,
                      obscureText: false,
                    ),
                    const SizedBox(height: 20),
                    _buildTextFormField(
                      hintText: "Password",
                      icon: Icons.lock,
                      obscureText: true,
                    ),
                    const SizedBox(height: 20),
                    SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: () {
                          // Handle login action
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color(0xFF6200EE),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          padding: const EdgeInsets.all(15),
                        ),
                        child: const Text(
                          'Login',
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    GestureDetector(
                      onTap: () {
                        // Handle Forgot Password action
                      },
                      child: const Text(
                        'Forget Password',
                        style: TextStyle(
                          color: Colors.white,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                    ),
                    const SizedBox(height: 10),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const SignUpMobile(),
                          ),
                        );
                      },
                      child: const Text(
                        "New to this site? Sign up..!",
                        style: TextStyle(
                          color: Colors.white,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    const Divider(
                      color: Colors.white,
                      thickness: 1,
                    ),
                    const SizedBox(height: 20),
                    const Footer(),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _buildTextFormField({
    required String hintText,
    required IconData icon,
    required bool obscureText,
  }) {
    return TextFormField(
      obscureText: obscureText,
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: const TextStyle(color: Colors.white),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
        suffixIcon: Icon(icon, color: Colors.white),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
          borderSide: const BorderSide(color: Colors.white),
        ),
      ),
      style: const TextStyle(color: Colors.white),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Please enter $hintText";
        }
        return null;
      },
    );
  }
}
