// import 'package:flutter/material.dart';
// import 'package:web_site/Home.dart';
// import 'package:web_site/LoginDesk.dart';
// import 'package:web_site/LoginMobile.dart';

// class SignUpMobile extends StatefulWidget {
//   const SignUpMobile({super.key});

//   @override
//   State<SignUpMobile> createState() => _SignUpDeskState();
// }

// class _SignUpDeskState extends State<SignUpMobile> {
//   @override
//   Widget build(BuildContext context) {
//     final isMobile = MediaQuery.of(context).size.width < 600;

//     return Scaffold(
//       body: LayoutBuilder(
//         builder: (context, constraints) {
//           return Container(
//             height: constraints.maxHeight,
//             color: const Color(0xFF0E0E2E),
//             child: Center(
//               child: SingleChildScrollView(
//                 child: Padding(
//                   padding:
//                       EdgeInsets.symmetric(horizontal: isMobile ? 16 : 100),
//                   child: Column(
//                     children: [
//                       const SizedBox(height: 20),
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: [
//                           Padding(
//                             padding: const EdgeInsets.all(16.0),
//                             child: Image.asset(
//                               'images/iinsparkeduskills.png',
//                               width: 100,
//                               height: 50,
//                             ),
//                           ),
//                         ],
//                       ),
//                       Image.asset(
//                         "images/signupman.png",
//                         width: 430,
//                         height: 320,
//                       ),
//                       const SizedBox(height: 20),
//                       const Text(
//                         "Register Now",
//                         style: TextStyle(
//                           fontSize: 30,
//                           fontWeight: FontWeight.w700,
//                           color: Colors.white,
//                         ),
//                       ),
//                       const SizedBox(height: 20),
//                       isMobile
//                           ? Column(
//                               children: _buildFormFields(isMobile),
//                             )
//                           : Row(
//                               children: [
//                                 Image.asset(
//                                   "images/signupman.png",
//                                   width: 430,
//                                   height: 320,
//                                 ),
//                                 const SizedBox(width: 20),
//                                 Flexible(
//                                   child: Column(
//                                     crossAxisAlignment:
//                                         CrossAxisAlignment.start,
//                                     children: _buildFormFields(isMobile),
//                                   ),
//                                 ),
//                               ],
//                             ),
//                       const SizedBox(height: 20),
//                       const SizedBox(
//                         width: double.infinity,
//                         child: Divider(
//                           color: Colors.white,
//                           thickness: 1,
//                         ),
//                       ),
//                       const Footer(),
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//           );
//         },
//       ),
//     );
//   }

//   List<Widget> _buildFormFields(bool isMobile) {
//     return [
//       _buildTextFormField(
//         hintText: "Full Name",
//         icon: Icons.person,
//         obscureText: false,
//         isMobile: isMobile,
//       ),
//       const SizedBox(height: 20),
//       _buildTextFormField(
//         hintText: "Email Address",
//         icon: Icons.email,
//         obscureText: false,
//         isMobile: isMobile,
//       ),
//       const SizedBox(height: 20),
//       _buildTextFormField(
//         hintText: "+91",
//         icon: Icons.phone,
//         obscureText: false,
//         isMobile: isMobile,
//       ),
//       const SizedBox(height: 20),
//       _buildTextFormField(
//         hintText: "Password",
//         icon: Icons.lock,
//         obscureText: true,
//         isMobile: isMobile,
//       ),
//       const SizedBox(height: 20),
//       _buildTextFormField(
//         hintText: "Confirm Password",
//         icon: Icons.lock,
//         obscureText: true,
//         isMobile: isMobile,
//       ),
//       const SizedBox(height: 20),
//       SizedBox(
//         width: isMobile ? double.infinity : 460,
//         child: ElevatedButton(
//           onPressed: () {
//             // Handle sign-up action
//           },
//           style: ElevatedButton.styleFrom(
//             backgroundColor: const Color(0xFF6200EE),
//             minimumSize: const Size(double.infinity, 50),
//             shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.circular(10),
//             ),
//           ),
//           child: const Text(
//             'Sign up',
//             style: TextStyle(
//               fontSize: 18,
//               color: Colors.white,
//               fontWeight: FontWeight.bold,
//             ),
//           ),
//         ),
//       ),
//       const SizedBox(height: 20),
//       GestureDetector(
//         onTap: () {
//           Navigator.push(
//             context,
//             MaterialPageRoute(builder: (context) => const LoginMobile()),
//           );
//         },
//         child: const Text(
//           'Already a member? Log in',
//           style: TextStyle(
//             color: Colors.white,
//             decoration: TextDecoration.underline,
//           ),
//         ),
//       ),
//     ];
//   }

//   Widget _buildTextFormField({
//     required String hintText,
//     required IconData icon,
//     required bool obscureText,
//     required bool isMobile,
//   }) {
//     return SizedBox(
//       width: isMobile ? double.infinity : 460,
//       child: TextFormField(
//         obscureText: obscureText,
//         decoration: InputDecoration(
//           hintText: hintText,
//           hintStyle: const TextStyle(color: Colors.white),
//           focusedBorder: const OutlineInputBorder(
//             borderSide: BorderSide(color: Colors.white),
//           ),
//           suffixIcon: Icon(icon, color: Colors.white),
//           border: OutlineInputBorder(
//             borderRadius: BorderRadius.circular(15),
//             borderSide: const BorderSide(color: Colors.white),
//           ),
//         ),
//         style: const TextStyle(color: Colors.white),
//         validator: (value) {
//           if (value == null || value.isEmpty) {
//             return "Please enter $hintText";
//           }
//           return null;
//         },
//       ),
//     );
//   }
// }

// class Footer extends StatelessWidget {
//   const Footer({super.key});

//   @override
//   Widget build(BuildContext context) {
//     final isMobile = MediaQuery.of(context).size.width < 600;

//     return Container(
//       padding: EdgeInsets.symmetric(
//         vertical: 20.0,
//         horizontal: isMobile ? 20.0 : 40.0,
//       ),
//       child: Column(
//         children: [
//           Row(
//             mainAxisAlignment: isMobile
//                 ? MainAxisAlignment.center
//                 : MainAxisAlignment.spaceBetween,
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Image.asset(
//                     'images/iinsparkeduskills.png',
//                     width: 90,
//                     height: 60,
//                   ),
//                   const SizedBox(height: 10),
//                   const Text(
//                     '''We are strategic & creative digital agency who are 
// focused on user experience, mobile, social, data 
// gathering and promotional offerings.''',
//                     style: TextStyle(
//                       color: Colors.white,
//                       fontSize: 14,
//                     ),
//                   ),
//                   const SizedBox(height: 25),
//                   Row(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: [
//                       GestureDetector(
//                         onTap: () {
//                           // Handle Instagram redirection
//                         },
//                         child: Image.asset(
//                           "images/instagram.png",
//                           height: 35,
//                           width: 35,
//                         ),
//                       ),
//                       const SizedBox(width: 15),
//                       GestureDetector(
//                         onTap: () {
//                           // Handle Twitter redirection
//                         },
//                         child: Image.asset(
//                           "images/twitter.png",
//                           height: 35,
//                           width: 35,
//                         ),
//                       ),
//                       const SizedBox(width: 15),
//                       GestureDetector(
//                         onTap: () {
//                           // Handle Facebook redirection
//                         },
//                         child: Image.asset(
//                           "images/fb.png",
//                           height: 35,
//                           width: 35,
//                         ),
//                       ),
//                     ],
//                   ),
//                   const SizedBox(height: 25),
//                   const Text(
//                     '© 2024 IINSPARK EDUSKILLS. All rights reserved.',
//                     style: TextStyle(
//                       color: Colors.white,
//                       fontSize: 14,
//                     ),
//                   ),
//                 ],
//               ),
//             ],
//           ),
//           const SizedBox(height: 20),
//         ],
//       ),
//     );
//   }
// }
