// // import 'package:flutter/material.dart';
// // import 'package:web_site/SignUpMobile.dart';

// // class LoginMobile extends StatefulWidget {
// //   const LoginMobile({Key? key}) : super(key: key);

// //   @override
// //   State<LoginMobile> createState() => _LoginMobileState();
// // }

// // class _LoginMobileState extends State<LoginMobile> {
// //   @override
// //   Widget build(BuildContext context) {
// //     return Scaffold(
// //       body: SingleChildScrollView(
// //         child: Container(
// //           color: const Color(0xFF0E0E2E),
// //           padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
// //           child: Column(
// //             crossAxisAlignment: CrossAxisAlignment.center,
// //             children: [
// //               Padding(
// //                 padding: EdgeInsets.only(right: 280),
// //                 child: Image.asset(
// //                   'images/iinsparkeduskills.png',
// //                   height: 60,
// //                   width: MediaQuery.of(context).size.width * 0.5,
// //                 ),
// //               ),
// //               const SizedBox(height: 20),
// //               // const Text(
// //               //   "Log In",
// //               //   style: TextStyle(
// //               //     fontSize: 24,
// //               //     fontWeight: FontWeight.w700,
// //               //     color: Colors.white,
// //               //   ),
// //               // ),
// //               const SizedBox(height: 20),
// //               Image.asset(
// //                 "images/signupman.png",
// //                 width: MediaQuery.of(context).size.width * 0.8,
// //               ),
// //               const SizedBox(height: 20),
// //               const Text(
// //                 "Log In",
// //                 style: TextStyle(
// //                   fontSize: 24,
// //                   fontWeight: FontWeight.w700,
// //                   color: Colors.white,
// //                 ),
// //               ),
// //               const SizedBox(height: 10),
// //               _buildTextFormField(
// //                 hintText: "Email Address",
// //                 icon: Icons.email,
// //                 obscureText: false,
// //               ),
// //               const SizedBox(height: 20),
// //               _buildTextFormField(
// //                 hintText: "Password",
// //                 icon: Icons.lock,
// //                 obscureText: true,
// //               ),
// //               const SizedBox(height: 20),
// //               SizedBox(
// //                 width: double.infinity,
// //                 child: ElevatedButton(
// //                   onPressed: () {
// //                     // Handle login action
// //                   },
// //                   style: ElevatedButton.styleFrom(
// //                     backgroundColor: const Color(0xFF6200EE),
// //                     shape: RoundedRectangleBorder(
// //                       borderRadius: BorderRadius.circular(10),
// //                     ),
// //                     padding: const EdgeInsets.all(15),
// //                   ),
// //                   child: const Text(
// //                     'Login',
// //                     style: TextStyle(
// //                       fontSize: 18,
// //                       color: Colors.white,
// //                       fontWeight: FontWeight.bold,
// //                     ),
// //                   ),
// //                 ),
// //               ),
// //               const SizedBox(height: 20),
// //               GestureDetector(
// //                 onTap: () {
// //                   // Handle Forgot Password action
// //                 },
// //                 child: const Text(
// //                   'Forget Password',
// //                   style: TextStyle(
// //                     color: Colors.white,
// //                     decoration: TextDecoration.underline,
// //                   ),
// //                 ),
// //               ),
// //               const SizedBox(height: 10),
// //               GestureDetector(
// //                 onTap: () {
// //                   Navigator.push(
// //                     context,
// //                     MaterialPageRoute(
// //                       builder: (context) => const SignUpMobile(),
// //                     ),
// //                   );
// //                 },
// //                 child: const Text(
// //                   "New to this site? Sign up..!",
// //                   style: TextStyle(
// //                     color: Colors.white,
// //                     decoration: TextDecoration.underline,
// //                   ),
// //                 ),
// //               ),
// //               const SizedBox(height: 20),
// //               const Divider(
// //                 color: Colors.white,
// //                 thickness: 1,
// //               ),
// //               const SizedBox(height: 20),
// //               const Padding(
// //                   padding: EdgeInsets.only(right: 90), child: Footer()),
// //             ],
// //           ),
// //         ),
// //       ),
// //     );
// //   }

// //   Widget _buildTextFormField({
// //     required String hintText,
// //     required IconData icon,
// //     required bool obscureText,
// //   }) {
// //     return TextFormField(
// //       obscureText: obscureText,
// //       decoration: InputDecoration(
// //         hintText: hintText,
// //         hintStyle: const TextStyle(color: Colors.white),
// //         focusedBorder: const OutlineInputBorder(
// //           borderSide: BorderSide(color: Colors.white),
// //         ),
// //         suffixIcon: Icon(icon, color: Colors.white),
// //         border: OutlineInputBorder(
// //           borderRadius: BorderRadius.circular(15),
// //           borderSide: const BorderSide(color: Colors.white),
// //         ),
// //       ),
// //       style: const TextStyle(color: Colors.white),
// //       validator: (value) {
// //         if (value == null || value.isEmpty) {
// //           return "Please enter $hintText";
// //         }
// //         return null;
// //       },
// //     );
// //   }
// // }

// // // class FooterMobile extends StatelessWidget {
// // //   const FooterMobile({Key? key}) : super(key: key);

// // //   @override
// // //   Widget build(BuildContext context) {
// // //     return Container(
// // //       padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 100),
// // //       child: Column(
// // //         crossAxisAlignment: CrossAxisAlignment.center,
// // //         children: [
// // //           Image.asset(
// // //             'images/iinsparkeduskills.png',
// // //             width: 120,
// // //             height: 80,
// // //           ),
// // //           const SizedBox(height: 10),
// // //           const Text(
// // //             '''We are strategic & creative digital agency who are
// // // focused on user experience, mobile, social, data
// // // gathering and promotional offerings.''',
// // //             textAlign: TextAlign.center,
// // //             style: TextStyle(
// // //               color: Colors.white,
// // //               fontSize: 12,
// // //             ),
// // //           ),
// // //           const SizedBox(height: 20),
// // //           Row(
// // //             mainAxisAlignment: MainAxisAlignment.center,
// // //             children: [
// // //               GestureDetector(
// // //                 onTap: () {
// // //                   // Handle Instagram redirection
// // //                 },
// // //                 child: Image.asset(
// // //                   "images/instagram.png",
// // //                   height: 30,
// // //                   width: 30,
// // //                 ),
// // //               ),
// // //               const SizedBox(width: 15),
// // //               GestureDetector(
// // //                 onTap: () {
// // //                   // Handle Twitter redirection
// // //                 },
// // //                 child: Image.asset(
// // //                   "images/twitter.png",
// // //                   height: 30,
// // //                   width: 30,
// // //                 ),
// // //               ),
// // //               const SizedBox(width: 15),
// // //               GestureDetector(
// // //                 onTap: () {
// // //                   // Handle Facebook redirection
// // //                 },
// // //                 child: Image.asset(
// // //                   "images/fb.png",
// // //                   height: 30,
// // //                   width: 30,
// // //                 ),
// // //               ),
// // //             ],
// // //           ),
// // //           const SizedBox(height: 20),
// // //           const Text(
// // //             '© 2024 IINSPARK EDUSKILLS. All rights reserved.',
// // //             style: TextStyle(
// // //               color: Colors.white,
// // //               fontSize: 12,
// // //             ),
// // //           ),
// // //         ],
// // //       ),
// // //     );
// // //   }
// // // }
// // class Footer extends StatelessWidget {
// //   const Footer({super.key});

// //   @override
// //   Widget build(BuildContext context) {
// //     final isMobile = MediaQuery.of(context).size.width < 600;

// //     return Container(
// //       padding: EdgeInsets.symmetric(
// //         vertical: 20.0,
// //         horizontal: isMobile ? 20.0 : 40.0,
// //       ),
// //       child: Column(
// //         children: [
// //           Row(
// //             mainAxisAlignment: isMobile
// //                 ? MainAxisAlignment.center
// //                 : MainAxisAlignment.spaceBetween,
// //             crossAxisAlignment: CrossAxisAlignment.start,
// //             children: [
// //               Column(
// //                 crossAxisAlignment: CrossAxisAlignment.start,
// //                 children: [
// //                   Image.asset(
// //                     'images/iinsparkeduskills.png',
// //                     width: 90,
// //                     height: 60,
// //                   ),
// //                   const SizedBox(height: 10),
// //                   const Text(
// //                     '''We are strategic & creative digital agency who are
// // focused on user experience, mobile, social, data
// // gathering and promotional offerings.''',
// //                     style: TextStyle(
// //                       color: Colors.white,
// //                       fontSize: 14,
// //                     ),
// //                   ),
// //                   const SizedBox(height: 25),
// //                   Row(
// //                     mainAxisAlignment: MainAxisAlignment.center,
// //                     children: [
// //                       GestureDetector(
// //                         onTap: () {
// //                           // Handle Instagram redirection
// //                         },
// //                         child: Image.asset(
// //                           "images/instagram.png",
// //                           height: 35,
// //                           width: 35,
// //                         ),
// //                       ),
// //                       const SizedBox(width: 15),
// //                       GestureDetector(
// //                         onTap: () {
// //                           // Handle Twitter redirection
// //                         },
// //                         child: Image.asset(
// //                           "images/twitter.png",
// //                           height: 35,
// //                           width: 35,
// //                         ),
// //                       ),
// //                       const SizedBox(width: 15),
// //                       GestureDetector(
// //                         onTap: () {
// //                           // Handle Facebook redirection
// //                         },
// //                         child: Image.asset(
// //                           "images/fb.png",
// //                           height: 35,
// //                           width: 35,
// //                         ),
// //                       ),
// //                     ],
// //                   ),
// //                   const SizedBox(height: 25),
// //                   const Text(
// //                     '© 2024 IINSPARK EDUSKILLS. All rights reserved.',
// //                     style: TextStyle(
// //                       color: Colors.white,
// //                       fontSize: 14,
// //                     ),
// //                   ),
// //                 ],
// //               ),
// //             ],
// //           ),
// //           const SizedBox(height: 20),
// //         ],
// //       ),
// //     );
// //   }
// // }

// import 'package:flutter/material.dart';
// import 'package:web_site/SignUpDesk.dart';
// import 'package:web_site/SignUpMobile.dart';

// class LoginMobile extends StatefulWidget {
//   const LoginMobile({Key? key}) : super(key: key);

//   @override
//   State<LoginMobile> createState() => _LoginMobileState();
// }

// class _LoginMobileState extends State<LoginMobile> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: LayoutBuilder(
//         builder: (context, constraints) {
//           return Container(
//             height: MediaQuery.of(context).size.height,
//             color: const Color(0xFF0E0E2E),
//             child: SingleChildScrollView(
//               child: Padding(
//                 padding: const EdgeInsets.symmetric(horizontal: 20),
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.center,
//                   children: [
//                     const SizedBox(height: 20),
//                     Image.asset(
//                       'images/iinsparkeduskills.png',
//                       width: 150,
//                       height: 75,
//                     ),
//                     const SizedBox(height: 20),
//                     const Text(
//                       "Log In",
//                       style: TextStyle(
//                         fontSize: 24,
//                         fontWeight: FontWeight.w700,
//                         color: Colors.white,
//                       ),
//                     ),
//                     const SizedBox(height: 20),
//                     Image.asset(
//                       "images/signupman.png",
//                       width: MediaQuery.of(context).size.width * 0.8,
//                     ),
//                     const SizedBox(height: 20),
//                     _buildTextFormField(
//                       hintText: "Email Address",
//                       icon: Icons.email,
//                       obscureText: false,
//                     ),
//                     const SizedBox(height: 20),
//                     _buildTextFormField(
//                       hintText: "Password",
//                       icon: Icons.lock,
//                       obscureText: true,
//                     ),
//                     const SizedBox(height: 20),
//                     SizedBox(
//                       width: double.infinity,
//                       child: ElevatedButton(
//                         onPressed: () {
//                           // Handle login action
//                         },
//                         style: ElevatedButton.styleFrom(
//                           backgroundColor: const Color(0xFF6200EE),
//                           shape: RoundedRectangleBorder(
//                             borderRadius: BorderRadius.circular(10),
//                           ),
//                           padding: const EdgeInsets.all(15),
//                         ),
//                         child: const Text(
//                           'Login',
//                           style: TextStyle(
//                             fontSize: 18,
//                             color: Colors.white,
//                             fontWeight: FontWeight.bold,
//                           ),
//                         ),
//                       ),
//                     ),
//                     const SizedBox(height: 20),
//                     GestureDetector(
//                       onTap: () {
//                         // Handle Forgot Password action
//                       },
//                       child: const Text(
//                         'Forget Password',
//                         style: TextStyle(
//                           color: Colors.white,
//                           decoration: TextDecoration.underline,
//                         ),
//                       ),
//                     ),
//                     const SizedBox(height: 10),
//                     GestureDetector(
//                       onTap: () {
//                         Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => const SignUpMobile(),
//                           ),
//                         );
//                       },
//                       child: const Text(
//                         "New to this site? Sign up..!",
//                         style: TextStyle(
//                           color: Colors.white,
//                           decoration: TextDecoration.underline,
//                         ),
//                       ),
//                     ),
//                     const SizedBox(height: 20),
//                     const Divider(
//                       color: Colors.white,
//                       thickness: 1,
//                     ),
//                     const SizedBox(height: 20),
//                     const Footer(),
//                   ],
//                 ),
//               ),
//             ),
//           );
//         },
//       ),
//     );
//   }

//   Widget _buildTextFormField({
//     required String hintText,
//     required IconData icon,
//     required bool obscureText,
//   }) {
//     return TextFormField(
//       obscureText: obscureText,
//       decoration: InputDecoration(
//         hintText: hintText,
//         hintStyle: const TextStyle(color: Colors.white),
//         focusedBorder: const OutlineInputBorder(
//           borderSide: BorderSide(color: Colors.white),
//         ),
//         suffixIcon: Icon(icon, color: Colors.white),
//         border: OutlineInputBorder(
//           borderRadius: BorderRadius.circular(15),
//           borderSide: const BorderSide(color: Colors.white),
//         ),
//       ),
//       style: const TextStyle(color: Colors.white),
//       validator: (value) {
//         if (value == null || value.isEmpty) {
//           return "Please enter $hintText";
//         }
//         return null;
//       },
//     );
//   }
// }

// class Footer extends StatelessWidget {
//   const Footer({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.center,
//         children: [
//           Image.asset(
//             'images/iinsparkeduskills.png', // Update this path
//             width: 100,
//             height: 50,
//           ),
//           const SizedBox(height: 10),
//           const Text(
//             '''We are strategic & creative digital agency who are 
// focused on user experience, mobile, social, data 
// gathering and promotional offerings.''',
//             textAlign: TextAlign.center,
//             style: TextStyle(
//               color: Colors.white,
//               fontSize: 12,
//             ),
//           ),
//           const SizedBox(height: 20),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               GestureDetector(
//                 onTap: () {
//                   // Handle Instagram redirection
//                 },
//                 child: Image.asset(
//                   "images/instagram.png",
//                   height: 30,
//                   width: 30,
//                 ),
//               ),
//               const SizedBox(width: 15),
//               GestureDetector(
//                 onTap: () {
//                   // Handle Twitter redirection
//                 },
//                 child: Image.asset(
//                   "images/twitter.png",
//                   height: 30,
//                   width: 30,
//                 ),
//               ),
//               const SizedBox(width: 15),
//               GestureDetector(
//                 onTap: () {
//                   // Handle Facebook redirection
//                 },
//                 child: Image.asset(
//                   "images/fb.png",
//                   height: 30,
//                   width: 30,
//                 ),
//               ),
//             ],
//           ),
//           const SizedBox(height: 20),
//           const Text(
//             '© 2024 IINSPARK EDUSKILLS. All rights reserved.',
//             style: TextStyle(
//               color: Colors.white,
//               fontSize: 12,
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
