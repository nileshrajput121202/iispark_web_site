// import 'package:flutter/material.dart';
// import 'package:web_site/SignUpDesk.dart';

// class LoginDesk extends StatefulWidget {
//   const LoginDesk({super.key});

//   @override
//   State<LoginDesk> createState() => _LoginDeskState();
// }

// class _LoginDeskState extends State<LoginDesk> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: LayoutBuilder(
//         builder: (context, constraints) {
//           return Container(
//             height: MediaQuery.sizeOf(context).height,
//             color: const Color(0xFF0E0E2E),
//             child: Center(
//               child: SingleChildScrollView(
//                 child: Padding(
//                   padding: const EdgeInsets.symmetric(horizontal: 35),
//                   child: Column(
//                     children: [
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: [
//                           Padding(
//                             padding: const EdgeInsets.all(16.0),
//                             child: Image.asset(
//                               'images/iinsparkeduskills.png',
//                               width: 100,
//                               height: 50,
//                             ),
//                           ),
//                         ],
//                       ),
//                       const SizedBox(height: 20),
//                       const Text(
//                         "Log In",
//                         style: TextStyle(
//                           fontSize: 30,
//                           fontWeight: FontWeight.w700,
//                           color: Colors.white,
//                         ),
//                       ),
//                       const SizedBox(height: 20),
//                       Row(
//                         children: [
//                           Image.asset(
//                             "images/signupman.png",
//                             width: 430,
//                             height: 320,
//                           ),
//                           const SizedBox(width: 20),
//                           Flexible(
//                             child: Column(
//                               crossAxisAlignment: CrossAxisAlignment.start,
//                               children: [
//                                 _buildTextFormField(
//                                   hintText: "Email Address",
//                                   icon: Icons.email,
//                                   obscureText: false,
//                                 ),
//                                 const SizedBox(height: 20),
//                                 _buildTextFormField(
//                                   hintText: "Password",
//                                   icon: Icons.lock,
//                                   obscureText: true,
//                                 ),
//                                 const SizedBox(height: 20),
//                                 SizedBox(
//                                   width: 460,
//                                   child: ElevatedButton(
//                                     onPressed: () {
//                                       // Handle login action
//                                     },
//                                     style: ElevatedButton.styleFrom(
//                                       backgroundColor: const Color(0xFF6200EE),
//                                       minimumSize:
//                                           const Size(double.infinity, 50),
//                                       shape: RoundedRectangleBorder(
//                                         borderRadius: BorderRadius.circular(10),
//                                       ),
//                                     ),
//                                     child: const Text(
//                                       'Login',
//                                       style: TextStyle(
//                                           fontSize: 18,
//                                           color: Colors.white,
//                                           fontWeight: FontWeight.bold),
//                                     ),
//                                   ),
//                                 ),
//                                 const SizedBox(height: 20),
//                                 GestureDetector(
//                                   onTap: () {
//                                     // Handle forget password
//                                   },
//                                   child: const Text(
//                                     'Forget Password',
//                                     style: TextStyle(
//                                       color: Colors.white,
//                                       decoration: TextDecoration.underline,
//                                     ),
//                                   ),
//                                 ),
//                                 const SizedBox(height: 10),
//                                 GestureDetector(
//                                   onTap: () {
//                                     Navigator.push(
//                                       context,
//                                       MaterialPageRoute(
//                                           builder: (context) =>
//                                               const SignUpDesk()),
//                                     );
//                                   },
//                                   child: const Text(
//                                     "New to this site? Sign up..!",
//                                     style: TextStyle(
//                                       color: Colors.white,
//                                       decoration: TextDecoration.underline,
//                                     ),
//                                   ),
//                                 ),
//                               ],
//                             ),
//                           ),
//                         ],
//                       ),
//                       const SizedBox(
//                         width: 2000, // Set the desired width
//                         child: Divider(
//                           color: Colors.white,
//                           thickness:
//                               1, // Optionally, you can also increase the thickness
//                         ),
//                       ),
//                       const Footer(),
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//           );
//         },
//       ),
//     );
//   }

//   Widget _buildTextFormField({
//     required String hintText,
//     required IconData icon,
//     required bool obscureText,
//   }) {
//     return SizedBox(
//       width: 460,
//       child: TextFormField(
//         obscureText: obscureText,
//         decoration: InputDecoration(
//           hintText: hintText,
//           hintStyle: const TextStyle(color: Colors.white),
//           focusedBorder: const OutlineInputBorder(
//             borderSide: BorderSide(color: Colors.white),
//           ),
//           suffixIcon: Icon(icon, color: Colors.white),
//           border: OutlineInputBorder(
//             borderRadius: BorderRadius.circular(15),
//             borderSide: const BorderSide(color: Colors.white),
//           ),
//         ),
//         style: const TextStyle(color: Colors.white),
//         validator: (value) {
//           if (value == null || value.isEmpty) {
//             return "Please enter $hintText";
//           }
//           return null;
//         },
//       ),
//     );
//   }
// }

// class Footer extends StatelessWidget {
//   const Footer({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 40.0),
//       child: Column(
//         children: [
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Image.asset(
//                     'images/iinsparkeduskills.png', // Update this path
//                     width: 90,
//                     height: 60,
//                   ),
//                   const SizedBox(height: 10),
//                   const Text(
//                     '''We are strategic & creative digital agency who are 
// focused on user experience, mobile, social, data 
// gathering and promotional offerings.''',
//                     style: TextStyle(
//                       color: Colors.white,
//                       fontSize: 14,
//                     ),
//                   ),
//                   const SizedBox(
//                     height: 25,
//                   ),
//                   Row(
//                     children: [
//                       GestureDetector(
//                         onTap: () {
//                           // Handle Instagram redirection
//                           // Example: You can use a package like url_launcher to open URLs
//                           // Make sure to import the package: import 'package:url_launcher/url_launcher.dart';
//                           // Example:
//                           // launch('https://www.instagram.com/your_instagram_account');
//                         },
//                         child: Image.asset(
//                           "images/instagram.png",
//                           height: 35,
//                           width: 35,
//                         ),
//                       ),
//                       const SizedBox(width: 15),
//                       GestureDetector(
//                         onTap: () {
//                           // Handle Twitter redirection
//                           // Example: You can use a package like url_launcher to open URLs
//                           // Make sure to import the package: import 'package:url_launcher/url_launcher.dart';
//                           // Example:
//                           // launch('https://twitter.com/your_twitter_account');
//                         },
//                         child: Image.asset(
//                           "images/twitter.png",
//                           height: 35,
//                           width: 35,
//                         ),
//                       ),
//                       const SizedBox(width: 15),
//                       GestureDetector(
//                         onTap: () {
//                           // Handle Facebook redirection
//                           // Example: You can use a package like url_launcher to open URLs
//                           // Make sure to import the package: import 'package:url_launcher/url_launcher.dart';
//                           // Example:
//                           // launch('https://www.facebook.com/your_facebook_account');
//                         },
//                         child: Image.asset(
//                           "images/fb.png",
//                           height: 35,
//                           width: 35,
//                         ),
//                       )
//                     ],
//                   ),
//                   const SizedBox(
//                     height: 25,
//                   ),
//                   const Text(
//                     '© 2024 IINSPARK EDUSKILLS. All rights reserved.',
//                     style: TextStyle(
//                       color: Colors.white,
//                       fontSize: 14,
//                     ),
//                   ),
//                 ],
//               ),
//             ],
//           ),
//           const SizedBox(
//             height: 20,
//           ),
//         ],
//       ),
//     );
//   }
// }
